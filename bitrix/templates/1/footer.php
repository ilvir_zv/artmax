<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
		</div>
	</div>

	<div id="footer" class="container p-5">
		<div class="row align-items-center justify-content-between">
			<div class="col-lg-3">
				<?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/company_name.php",
				Array(),
				Array("MODE"=>"html")
				);
				?>
			</div>
			<div class="col-lg-3">
				<?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/contacts.php",
				Array(),
				Array("MODE"=>"html")
				);
				?>
			</div>
			<div class="col-lg-3">
				<?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/social.php",
				Array(),
				Array("MODE"=>"html")
				);
				?>
			</div>
			<div class="col-lg-3">
				<?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/artmax.php",
				Array(),
				Array("MODE"=>"html")
				);
				?>
			</div>
		</div>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.maskedinput.js"></script>
	<script>
	$(document).ready(function() {
			$("#inputPhone").mask("+7 (999) 99-99-999");
		})

		var top_show = 150; // В каком положении полосы прокрутки начинать показ кнопки "Наверх"
		var delay = 1000; // Задержка прокрутки
		$(document).ready(function() {
			$(window).scroll(function () { // При прокрутке попадаем в эту функцию
				/* В зависимости от положения полосы прокрукти и значения top_show, скрываем или открываем кнопку "Наверх" */
				if ($(this).scrollTop() > top_show) $('#top').fadeIn();
				else $('#top').fadeOut();
			});
			$('#top').click(function () { // При клике по кнопке "Наверх" попадаем в эту функцию
				/* Плавная прокрутка наверх */
				$('body, html').animate({
					scrollTop: 0
				}, delay);
			});
		});

	$(function() {
			$('#submit').click(function(e) {
				var $form = $('#contactUs');
				$.ajax({
					type: 'POST',
					url: '/ajax/contactus.php',
					data: { name: $("#inputName").val(), phone: $("#inputPhone").val(), city: $("#inputCity").val(), comment: $("#inputComments").val() }
				}).done(function(data) {
					console.log(data);
				}).fail(function() {
					console.log('fail');
				});
				//отмена действия по умолчанию для кнопки submit
				e.preventDefault();
			});
		});
	</script>
</body>
</html>
