<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?$APPLICATION->ShowHead();?>
<link href="<?=SITE_TEMPLATE_PATH?>/common.css" type="text/css" rel="stylesheet" />
<link href="<?=SITE_TEMPLATE_PATH?>/colors.css" type="text/css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

	<!--[if lte IE 6]>
	<style type="text/css">

		#banner-overlay {
			background-image: none;
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>images/overlay.png', sizingMethod = 'crop');
		}

		div.product-overlay {
			background-image: none;
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>images/product-overlay.png', sizingMethod = 'crop');
		}

	</style>
	<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
	<div id="page-wrapper">
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
		<div id="header">
			<nav class="navbar navbar-expand-lg navbar-light">
			  <a class="navbar-brand" href="<?=SITE_DIR?>" title="<?=GetMessage('CFT_MAIN')?>"><?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/company_name.php",
				Array(),
				Array("MODE"=>"html")
				);
				?></a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>

			  <div class="collapse navbar-collapse" id="navbarSupportedContent">
					<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"top",
	array(
		"ROOT_MENU_TYPE" => "top",
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "top",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);?>

					<ul class="navbar-nav">
						<li class="nav-item ">
							<a class="nav-link" href="#"><?
																			$APPLICATION->IncludeFile(
																			SITE_DIR."include/fb.php",
																			Array(),
																			Array("MODE"=>"html")
																			);
																			?></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="#"><?
																			$APPLICATION->IncludeFile(
																			SITE_DIR."include/inst.php",
																			Array(),
																			Array("MODE"=>"html")
																			);
																			?></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="#"><?
																			$APPLICATION->IncludeFile(
																			SITE_DIR."include/email.php",
																			Array(),
																			Array("MODE"=>"html")
																			);
																			?></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#"><?
																			$APPLICATION->IncludeFile(
																			SITE_DIR."include/tel.php",
																			Array(),
																			Array("MODE"=>"html")
																			);
																			?></a>
						</li>
					</ul>
			  </div>

			</nav>

		</div>


<?
$APPLICATION->IncludeFile(
	SITE_DIR."include/motto.php",
	Array(),
	Array("MODE"=>"html")
);
?>


		<div id="content">
			<div id="workarea">
