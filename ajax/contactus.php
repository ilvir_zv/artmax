<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?

 CModule::IncludeModule("iblock");
$el = new CIBlockElement;

$PROP = array();
$PROP['phone'] = $_POST['phone'];
$PROP['city'] = $_POST['city'];
$PROP['comment'] = $_POST['comment'];

$arLoadProductArray = Array(
  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
  "IBLOCK_ID"      => 6,
  "PROPERTY_VALUES"=> $PROP,
  "NAME"           => $_POST['name'],
  "ACTIVE"         => "Y",            // активен
  "PREVIEW_TEXT"   => $_POST['comment']
  );

if($PRODUCT_ID = $el->Add($arLoadProductArray))
  echo 'good';
else
  echo "Error: ".$el->LAST_ERROR;
?>
