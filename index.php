<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("ArtMax - mirrors & glasses");
?><div class="container contPaddings">
	<div class="container-title">
		 Try our service in business
	</div>
	<div class="container-desc">
		 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lig
	</div>

	<?$APPLICATION->IncludeComponent("bitrix:news.list", "services", Array(
	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "7",	// Код информационного блока
		"IBLOCK_TYPE" => "service",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "6",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
	),
	false
);?>

</div>
<div class="container-fluid contPaddings" style="background: #F7F9FD;">
	<div class="container-title">
		 How do we work?
	</div>
	<div class="container-desc">
		 We use a new method to solve problems
	</div>
	<div class="container">
		 <?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/howDoWeWork.php",
				Array(),
				Array("MODE"=>"html")
				);
				?>
	</div>
</div>
<div class="container contPaddings whyUs">
	<div class="container-title">
		 Why most people choose us
	</div>
	<div class="row m-4">
		 <?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/whyUs/layers.php",
				Array(),
				Array("MODE"=>"html")
				);
				?> <?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/whyUs/arrow.php",
				Array(),
				Array("MODE"=>"html")
				);
				?> <?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/whyUs/cycle.php",
				Array(),
				Array("MODE"=>"html")
				);
				?>
		<div class="w-100">
		</div>
		 <?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/whyUs/eye.php",
				Array(),
				Array("MODE"=>"html")
				);
				?> <?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/whyUs/free.php",
				Array(),
				Array("MODE"=>"html")
				);
				?> <?
				$APPLICATION->IncludeFile(
				SITE_DIR."include/whyUs/expertise.php",
				Array(),
				Array("MODE"=>"html")
				);
				?>
	</div>
</div>
<div class="container contPaddings feedback">
	<div class="container-title">
		 Feedback
	</div>
	<div class="container-desc">
		 These people have already used our services and have shared their impressions
	</div>
	<div class="row">
		<div class="col-12 col-lg-9" style="margin: 3rem auto;">
			<?$APPLICATION->IncludeComponent("bitrix:news.list", "feedback", Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
			"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
			"AJAX_MODE" => "N",	// Включить режим AJAX
			"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
			"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
			"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
			"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
			"CACHE_GROUPS" => "Y",	// Учитывать права доступа
			"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
			"CACHE_TYPE" => "A",	// Тип кеширования
			"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
			"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
			"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
			"DISPLAY_DATE" => "Y",	// Выводить дату элемента
			"DISPLAY_NAME" => "Y",	// Выводить название элемента
			"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
			"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
			"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
			"FIELD_CODE" => array(	// Поля
				0 => "ID",
				1 => "CODE",
				2 => "XML_ID",
				3 => "NAME",
				4 => "TAGS",
				5 => "SORT",
				6 => "PREVIEW_TEXT",
				7 => "PREVIEW_PICTURE",
				8 => "DETAIL_TEXT",
				9 => "DETAIL_PICTURE",
				10 => "DATE_ACTIVE_FROM",
				11 => "ACTIVE_FROM",
				12 => "DATE_ACTIVE_TO",
				13 => "ACTIVE_TO",
				14 => "SHOW_COUNTER",
				15 => "SHOW_COUNTER_START",
				16 => "IBLOCK_TYPE_ID",
				17 => "IBLOCK_ID",
				18 => "IBLOCK_CODE",
				19 => "IBLOCK_NAME",
				20 => "IBLOCK_EXTERNAL_ID",
				21 => "DATE_CREATE",
				22 => "CREATED_BY",
				23 => "CREATED_USER_NAME",
				24 => "TIMESTAMP_X",
				25 => "MODIFIED_BY",
				26 => "USER_NAME",
				27 => "",
			),
			"FILTER_NAME" => "",	// Фильтр
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
			"IBLOCK_ID" => "5",	// Код информационного блока
			"IBLOCK_TYPE" => "reviews",	// Тип информационного блока (используется только для проверки)
			"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
			"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
			"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
			"NEWS_COUNT" => "20",	// Количество новостей на странице
			"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
			"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
			"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
			"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
			"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
			"PAGER_TITLE" => "Новости",	// Название категорий
			"PARENT_SECTION" => "",	// ID раздела
			"PARENT_SECTION_CODE" => "",	// Код раздела
			"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
			"PROPERTY_CODE" => array(	// Свойства
				0 => "",
				1 => "",
			),
			"SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
			"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
			"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
			"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
			"SET_STATUS_404" => "N",	// Устанавливать статус 404
			"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
			"SHOW_404" => "N",	// Показ специальной страницы
			"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
			"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
			"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
			"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
			"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		),
		false
	);?>
		</div>
	</div>
</div>

<div class="container-fluid contPaddings contactUs" style="background: url(<?=SITE_TEMPLATE_PATH?>/images/info-diamant-1.png) 100% 100%;">
	<div class="container">
		<div class="title mb-5">
			 Contact us
		</div>
		<div class="row">
			<div class="col-12 col-lg-6">
				<form id="contactUs">
				  <div class="form-row">
				    <div class="form-group col-md-6">
				      <input type="text" class="form-control" id="inputName" placeholder="Name">
				    </div>
				    <div class="form-group col-md-6">
				      <input type="text" class="form-control" id="inputPhone" placeholder="Phone">
				    </div>
				  </div>
				  <div class="form-group">
				    <input type="text" class="form-control" id="inputCity" placeholder="City">
				  </div>
				  <div class="form-group">
				    <input type="text" class="form-control" id="inputComments" placeholder="Comments">
				  </div>
				  <input type="button" id="submit" class="btn btn-primary submit-button" value="Submit">
				</form>
			</div>
		</div>
	</div>
</div>
<div id="top">
	Up <img src="<?=SITE_TEMPLATE_PATH?>/images/ArrowToTop.png" />
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
