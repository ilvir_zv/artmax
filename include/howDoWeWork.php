<div class="row m-4 justify-content-between">
  <div class="col-12 col-lg-3 p-3">
    <div class="row align-items-center">
      <div class="col-12 howDoWeWork-title">
        01
      </div>
      <div class="col-12 howDoWeWork-desc">
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
      </div>
        <img src="<?=SITE_TEMPLATE_PATH?>/images/Arrow01.png" class="howDoWeWork-arrow"/>
    </div>
  </div>
  <div class="col-12 col-lg-3 p-3">
    <div class="row align-items-center">
      <div class="col-12 howDoWeWork-title">
        02
      </div>
      <div class="col-12 howDoWeWork-desc">
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
      </div>
        <img src="<?=SITE_TEMPLATE_PATH?>/images/Arrow02.png" class="howDoWeWork-arrow"/>
    </div>
  </div>
  <div class="col-12 col-lg-3 p-3">
    <div class="row align-items-center">
      <div class="col-12 howDoWeWork-title">
        03
      </div>
      <div class="col-12 howDoWeWork-desc">
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
      </div>
    </div>
  </div>
</div>
